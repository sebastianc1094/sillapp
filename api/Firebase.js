import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyC82vKQbsiI01NWxjbaFcCJNGBEmlqzd3g",
  authDomain: "sillapp-d78da.firebaseapp.com",
  databaseURL: "https://sillapp-d78da.firebaseio.com",
  projectId: "sillapp-d78da",
  storageBucket: "sillapp-d78da.appspot.com",
  messagingSenderId: "346587199572",
};

const firebaseAPI = firebase.initializeApp(config);

export const firebaseAuth = firebase.auth();
export const firebaseDatabase = firebase.database();

export default firebaseAPI
