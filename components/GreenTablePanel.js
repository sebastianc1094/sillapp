import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView, ListView } from 'react-native';

import GreenTable from './GreenTable'

export default class GreenTablePanel extends React.Component {
  constructor(props){
    super(props);
    const data = Array(20).fill(GreenTable).map(table => {
      return <GreenTable />
    })
    //for (let i = 0; i < 50; i++){
    //  data.push(i);
    //}
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(data),
    }
  }

  _renderRow(data){
    return(
      <View style = {styles.box}>
        {data}
      </View>
    );
  }

  render() {
    return (

        <View style = {styles.panel}>
          <ListView renderRow = {this._renderRow.bind(this)}
          dataSource = {this.state.dataSource}
          contentContainerStyle = {{flexDirection: 'row', flexWrap: 'wrap'}}
          pageSize = {this.data}/>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'blue'
  },
  panel: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15,
    backgroundColor: 'transparent'
  }
});
