import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import { firebaseDatabase, firebaseAuth } from '../api/Firebase';

export default class GreenTable extends React.Component {
  state = {
    empty: false,
    sit: false
  }

  handlePress = () => {
    this.setState({ empty: !this.state.empty });

    this.toggleTouch(this.state.empty);
  }

  getTableRef = () => {
    return firebaseDatabase.ref(`Green`);
  }

  toggleTouch = (sit) => {
    const {uid} = firebaseAuth.currentUser

      this.getTableRef().transaction(function(table) {
        if (table) {
          if (table.sit && table.sit[uid]) {
            table.sit[uid] = null;
          } else {
            if (!table.sit) {
              table.sit = {};
            }
            table.sit[uid] = true;
          }
        }
        return table || {
          sit:{
            [uid]: true
          }
        };
      });
    }

  render() {
    const emptyIcon = this.state.empty ?
    <Icon name="ios-man" size={50} color='red'/> :
    <Icon name="ios-pin" size={50} color='green'/>

    return (
      <View style = {styles.container}>
        <View style = {styles.table}>
          <TouchableOpacity onPress = {this.handlePress}>
            { emptyIcon }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    margin: 30,
  },
  table:{
    width: 100,
    height: 50,
    backgroundColor: '#BDBDBD',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
