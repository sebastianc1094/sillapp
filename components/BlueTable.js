import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import store from "./globals.js";
import { firebaseDatabase, firebaseAuth } from '../api/Firebase';

export default class BlueTable extends React.Component {
    state = {
      use: false,
    }

  handlePress = () => {

    if(store.lockedChair || store.lockedChair == null){ // cuando no estoy sentando en ninguna debo bloquear alguna , null es cuando va hacer la primera vez y no he bloqueado ninguna
      store.lockedChair = false;
      this.setState({ use: !this.state.use });
      this.toggleTouch();
    }else if(this.state.use) { // cuando estoy sentando ya en alguna la puedo desbloquear
      store.lockedChair = true;
      this.setState({ use: !this.state.use });
      this.toggleTouch();
    }else{ // cuando ya estoy sentando no puedo usar mas !
      console.log("ESTADO","BLOQUEADO !"+store.lockedChair);
    }
  }

  getTableRef = () => {
    return firebaseDatabase.ref(`Blue`);
  }

  toggleTouch = (sit) => {
    const {uid} = firebaseAuth.currentUser;
      this.getTableRef().transaction(function(table) {
        if (table) {
          if (table.sit && table.sit[uid]) {
            table.sit[uid] = null;
          } else {
            if(!table.sit){
              table.sit = {};
            }
            table.sit[uid] = true;
          }
        }
        return table || {
          sit:{
            [uid]: true,
          }
        };
      });
    }

  render() {
    const {uid} = firebaseAuth.currentUser;

    const useIcon = this.state.use ?
    <Icon name="ios-man" size={50} color='red'/> :
    <Icon name="ios-pin" size={50} color='green'/>

    const user = this.state.use ?
    <Text style={{fontSize:6}}>{uid}</Text> :
    <Text>Libre</Text>

    return (
      <View style = {styles.container}>
        <View style = {styles.table}>
          <TouchableOpacity onPress = {this.handlePress}>
            { useIcon }
          </TouchableOpacity>
          {user}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    margin: 30,
  },
  table:{
    width: 100,
    height: 80,
    backgroundColor: '#BDBDBD',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
