import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { StackNavigator, NavigationActions } from 'react-navigation';

import LoginScreen from './screens/auth/LoginScreen';
import RegisterScreen from './screens/auth/RegisterScreen';

import HomeScreen from './screens/app/HomeScreen';
import BlueScreen from './screens/app/BlueScreen';
import GreenScreen from './screens/app/GreenScreen';
import OrangeScreen from './screens/app/OrangeScreen';
import YellowScreen from './screens/app/YellowScreen';

export default class App extends React.Component {
  constructor() {
    super();
    console.ignoredYellowBox = ['Setting a timer'];
  }

  componentWillMount() {
    StatusBar.setHidden(true);
  }

  render() {
    return (
      <AuthStack />
    );
  }
}

const AppStack = StackNavigator({
  HomeScreen: { screen: HomeScreen,
    navigationOptions:{header:null}},
  BlueScreen: { screen: BlueScreen },
  GreenScreen: { screen: GreenScreen },
  OrangeScreen: { screen: OrangeScreen },
  YellowScreen: { screen: YellowScreen },
});

const AuthStack = StackNavigator({
  LoginScreen: {screen: LoginScreen,
    navigationOptions:{header:null}},
  RegisterScreen: { screen: RegisterScreen },
  AppStack: { screen: AppStack,
    navigationOptions:{header:null}},
});

const blockHome = AppStack.router.getStateForAction;
AppStack.router.getStateForAction = (action, state) => {
  if (state && action.type === NavigationActions.BACK && state.routes[state.index].routeName === "HomeScreen") {
    return null;
  }
  return blockHome(action, state);
};

const blockAuth = AuthStack.router.getStateForAction;
AuthStack.router.getStateForAction = (action, state) => {
  if (state && action.type === NavigationActions.BACK && state.routes[state.index].routeName === "LoginScreen"){
    return null
  }
  return blockAuth(action, state);
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
