import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Alert, TouchableOpacity, KeyboardAvoidingView } from 'react-native';

import firebaseAPI from '../../api/Firebase';
import Icon from 'react-native-vector-icons/Ionicons';

export default class LoginScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {email:'',password:''};
  }

  static navigationOptions = {
    title: 'Bienvenido',
  };

  onLoginPress(){
    const{email,password} = this.state;
    firebaseAPI.auth().signInWithEmailAndPassword(email, password)
    .then(() => {this.props.navigation.navigate('HomeScreen')}).catch((error) => {
      alert('Datos incorrectos');
    })
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView behavior="padding" style={{flex:1}}>
        <View style={styles.container}>
          <View style={styles.formContainer}>
              <Text style = {{ textAlign: 'center', fontSize: 35}}>SillApp</Text>
              <View style = {styles.icons}>
                <View style = {styles.free}>
                  <Icon name="ios-pin" size={50} color='green'/>
                </View>
                <View style = {styles.full}>
                  <Icon name="ios-man" size={50} color='red'/>
                </View>
              </View>
              <View>
                <Text style = {{ textAlign: 'center', fontSize: 20}}>Correo</Text>
                <TextInput style={styles.inputs} value = {this.state.email} onChangeText = {email => this.setState({email})}
                  placeholder = "   correo@upb.edu.co" underlineColorAndroid = 'transparent'/>
              </View>
              <View>
                <Text style = {{ textAlign: 'center', fontSize: 20}}>Contraseña</Text>
                <TextInput style={styles.inputs} value = {this.state.password} onChangeText = {password => this.setState({password})}
                  secureTextEntry placeholder="   Contraseña" underlineColorAndroid='transparent'/>
              </View>
              <View style = {styles.button}>
                <Button onPress = {this.onLoginPress.bind(this)} title="Ingresar" color="#CFD8DC"/>
              </View>
              <View style={{alignItems:'center', justifyContent:'center'}}>
                <TouchableOpacity activeOpacity={.5} onPress={() => navigate('RegisterScreen')}>
                  <Text>Registrarse</Text>
                </TouchableOpacity>
              </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  formContainer:{
    marginTop: 100
  },
  icons:{
    flexDirection: 'row'
  },
  free:{
    flex: 1,
    alignItems: 'center'
  },
  full:{
    flex: 1,
    alignItems: 'center'
  },
  inputs:{
    height: 40,
    borderColor: 'gray',
    borderWidth: .5,
    borderRadius: 20,
    margin: 25,
    marginTop: 10
  },
  button:{
    margin: 45,
    marginTop: 10
  }
});
