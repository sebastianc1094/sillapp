import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Alert } from 'react-native';

import firebaseAPI from '../../api/Firebase';

export default class RegisterScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {email:'',password:''};
  }

  static navigationOptions = {
    title: 'Registrarse',
  };

  onSignUpPress(){
    const{email,password} = this.state;
    firebaseAPI.auth().createUserWithEmailAndPassword(email, password)
    .then(() => {alert('Cuenta creada')})
    .catch((error) => {
      alert('Datos incorrectos, asegúrese de haber escrito un correo válido o que su contraseña sea de 6 caracteres o más');
    }).then(() => {this.props.navigation.navigate('LoginScreen')})
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <View>
            <Text style = {{ textAlign: 'center', fontSize: 20}}>Correo</Text>
            <TextInput style={styles.inputs} value = {this.state.email} onChangeText = {email => this.setState({email})}
            placeholder = "   correo@upb.edu.co" underlineColorAndroid = 'transparent'/>
          </View>
          <View>
            <Text style = {{ textAlign: 'center', fontSize: 20}}>Contraseña</Text>
            <TextInput style={styles.inputs} value = {this.state.password} onChangeText = {password => this.setState({password})}
             secureTextEntry placeholder="   Contraseña" underlineColorAndroid='transparent'/>
          </View>
          <View style = {styles.button}>
            <Button onPress={this.onSignUpPress.bind(this)} title="Registrarse" color="#303F9F"/>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  formContainer:{
    marginTop: 50
  },
  inputs:{
    height: 40,
    borderColor: 'gray',
    borderWidth: .5,
    borderRadius: 20,
    margin: 25,
    marginTop: 10
  },
  button:{
    margin: 45,
    marginTop: 10
  }
});
