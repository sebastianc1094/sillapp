import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import YellowTablePanel from '../../components/YellowTablePanel';

export default class YellowScreen extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <YellowTablePanel />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFF00',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
