import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import {firebaseAuth, firebaseAPI} from '../../api/Firebase';

import Icon from 'react-native-vector-icons/Ionicons';

export default class HomeScreen extends React.Component {
  logout() {
    firebaseAuth.signOut().then(() => {
      this.props.navigation.navigate('LoginScreen');
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    const {uid} = firebaseAuth.currentUser;
    return (
      <View style = {styles.container}>
        <Text style = {{textAlign: 'center',fontSize:30}}>Bienvenido</Text>
        <Text style = {{textAlign: 'center',fontSize:15}}>Seleccione la zona donde se quiere sentar</Text>
        <View style = {styles.blue}>
          <Button onPress={() => navigate('BlueScreen')} title="Techo azul" color="#303F9F"/>
        </View>
        <View style = {styles.green}>
          <Button onPress={() => navigate('GreenScreen')} title="Techo verde" color="#43A047"/>
        </View>
        <View style = {styles.yellow}>
          <Button onPress={() => navigate('OrangeScreen')} title="Techo naranja" color="#FF6D00"/>
        </View>
        <View style = {styles.orange}>
          <Button style = {{ color: 'black'}} onPress={() => navigate('YellowScreen')} title="Techo amarillo" color="#FFFF00"/>
        </View>
        <View style = {styles.title}>
          <Text style = {{ textAlign:'center', fontSize:25 }}>Ayuda</Text>
        </View>
        <View style = {styles.icons}>
          <View style = {styles.free}>
            <Icon name="ios-pin" size={50} color='green'/>
            <Text>Mesa libre</Text>
          </View>
          <View style = {styles.full}>
            <Icon name="ios-man" size={50} color='red'/>
            <Text>Mesa ocupada</Text>
          </View>
        </View>
        <View style = {styles.logout}>
          <Text style = {{textAlign: 'center'}}>Usuario activo:</Text>
          <Text style = {{textAlign: 'center'}}>{uid}</Text>
          <Button onPress={this.logout.bind(this)} title="Cerrar sesión" color="red"/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    margin: 25,
    marginTop: 50
  },
  free:{
    flex: 1,
    alignItems: 'center'
  },
  full:{
    flex: 1,
    alignItems: 'center'
  },
  icons:{
    flexDirection: 'row'
  },
  title:{
    marginBottom: 10
  },
  blue:{
    margin: 10
  },
  green:{
    margin: 10
  },
  orange:{
    margin: 10
  },
  yellow:{
    margin: 10
  },
  logout:{
    marginTop: 40
  }
});
