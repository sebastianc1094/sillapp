import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import GreenTablePanel from '../../components/GreenTablePanel';

export default class GreenScreen extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <GreenTablePanel />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#43A047',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
