import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import OrangeTablePanel from '../../components/OrangeTablePanel';

export default class OrangeScreen extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <OrangeTablePanel />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FF6D00',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
