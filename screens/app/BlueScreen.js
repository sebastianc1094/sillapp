import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import BlueTablePanel from '../../components/BlueTablePanel';

export default class BlueScreen extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <BlueTablePanel />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A237E',
  },
});
